$(document).ready(function() {

	$(".carousel__list").customCarousel({
		nextSelector: $(".carousel__arrow_next"),
        prevSelector: $(".carousel__arrow_prev"),
        changeSelector: ".carousel__control-item",
        afterSlideCallback: function(ui){
        	var self = $(ui.carouselItems.context).closest(".carousel");
        	self.find(".carousel__control-item").removeClass("carousel__control-item_active");
        	self.find(".carousel__control-item").eq(ui.currentIndex).addClass("carousel__control-item_active");
        }
	});

    $(".gallery__list").customCarousel({
        nextSelector: $(".gallery__arrow_next"),
        prevSelector: $(".gallery__arrow_prev"),
        afterSlideCallback: function(ui){
            $(".gallery__index").text(ui.currentIndex + 1)
        }
    });

    $(".carousel__search-button").click(function(){
        $(this).closest(".carousel__search").addClass("carousel__search_active");
    });

    $(".sert__item, .gallery__link").fancybox();

    if ($("#map").length > 0){
        ymaps.ready(init);
        var myMap;

        function init(){     
            myMap = new ymaps.Map("map", {
                center: [55.1775,61.3264],
                zoom: 15,
                controls: []
            });

            myPlacemark = new ymaps.Placemark([55.1775,61.3264], {
                hintContent: 'Premier Clinik'
            });

            myMap.geoObjects.add(myPlacemark);
        }
    }
	
});